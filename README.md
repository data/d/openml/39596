# OpenML dataset: QSAR-DATASET-FOR-DRUG-TARGET-CHEMBL1075097

https://www.openml.org/d/39596

**WARNING: This dataset is archived. Use [meta](https://gitlab.com/data/meta) to discuss it.**

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

This dataset contains QSAR data (from ChEMBL version 17) showing activity values (unit is pseudo-pCI50) of several compounds on drug target ChEMBL_ID: CHEMBL1075097 (TID: 103147), and it has 35 rows and 1024 features (not including molecule IDs and class feature: molecule_id and pXC50). The features represent FCFP 1024-bit Molecular Fingerprints which were generated from SMILES strings. They were obtained using the Pipeline Pilot program, Dassault Systèmes BIOVIA. Generating Fingerprints do not usually require missing value imputation as all bits are generated.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/39596) of an [OpenML dataset](https://www.openml.org/d/39596). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/39596/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/39596/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/39596/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

